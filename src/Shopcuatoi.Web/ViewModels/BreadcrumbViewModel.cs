﻿namespace Shopcuatoi.Web.ViewModels
{
    public class BreadcrumbViewModel
    {
        public string Text { get; set; }

        public string Url { get; set; }
    }
}
