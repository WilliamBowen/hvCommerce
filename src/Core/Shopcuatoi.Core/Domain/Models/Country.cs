﻿using Shopcuatoi.Infrastructure.Domain.Models;

namespace Shopcuatoi.Core.Domain.Models
{
    public class Country : Entity
    {
        public string Name { get; set; }
    }
}